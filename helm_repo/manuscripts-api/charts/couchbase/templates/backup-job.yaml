{{- if eq .Values.global.gke true }}
# Create a PV claim
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{ .Release.Name }}-cb-backup-data-claim
  labels:
    identifier: {{ .Release.Name }}-bk-pv
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: standard-retain
  resources:
    requests:
      # Use the disk sum of all nodes as backup disk size. In order to calculate we need to split
      # We extract the fload from the pv storage size (and convert to float), then round to 0 decimal
      # and then multiply with the number of nodes
      # Then we combine the number with the measurement (Gi,Mi,..) and remove intermediate spaces
      storage: {{ nospace (cat (round ((regexFind "[0-9.]+" .Values.cluster.pv_storage_size)|float64) 0|mul .Values.cluster.servers) (regexFind "[a-zA-Z]+" .Values.cluster.pv_storage_size))  }}
---
# Create a backup repository (inital step needed)
kind: Job
apiVersion: batch/v1
metadata:
  name: {{ .Release.Name }}-cb-backup-initialize
spec:
  backoffLimit: 0
  template:
    metadata:
      labels:
        type: cb-backup
    spec:
      {{- if .Values.global.nodePool }}
      nodeSelector:
        cloud.google.com/gke-nodepool: {{ .Values.nodeSpecs.nodeType }}-{{ .Release.Namespace }}-pool
      {{- end }}
      {{- if .Values.global.taint }}
      tolerations:
        - key: {{ .Release.Namespace }}
          operator: Exists
          effect: NoSchedule
      {{- end }}
      affinity:
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: type
                operator: In
                values:
                - cb-backup
            topologyKey: "kubernetes.io/hostname"
      containers:
        - name: {{ .Release.Name }}-cb-backup-initialize
          image: {{ .Values.cluster.image }}
          version: {{ .Values.cluster.tag }}
          command: ["cbbackupmgr", "config", "--archive", "/backups/cb", "--repo", "couchbase"]
          volumeMounts:
            - name: {{ .Release.Name }}-cb-backup-data
              mountPath: "/backups"
      restartPolicy: Never
      volumes:
        - name: {{ .Release.Name }}-cb-backup-data
          persistentVolumeClaim:
            claimName: {{ .Release.Name }}-cb-backup-data-claim
---
# The cron job should run after the above step
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: "{{ .Release.Name }}-cb-backup"
spec:
  backoffLimit: 0
  failedJobsHistoryLimit: 3
  successfulJobsHistoryLimit: 3
  schedule: "19 1 * * *" # Daily
  concurrencyPolicy: Forbid # Disallow 2 backup jobs to run in parallel
  jobTemplate:
    spec:
      backoffLimit: 0
      template:
        metadata:
          labels:
            type: cb-backup
        spec:
          {{- if .Values.global.nodePool }}
          nodeSelector:
            cloud.google.com/gke-nodepool: {{ .Values.nodeSpecs.nodeType }}-{{ .Release.Namespace }}-pool
          {{- end }}
          {{- if .Values.global.taint }}
          tolerations:
            - key: {{ .Release.Namespace }}
              operator: Exists
              effect: NoSchedule
          {{- end }}
          affinity:
            podAffinity:
              requiredDuringSchedulingIgnoredDuringExecution:
              - labelSelector:
                  matchExpressions:
                  - key: type
                    operator: In
                    values:
                    - cb-backup
                topologyKey: "kubernetes.io/hostname"
          containers:
          - name: "{{ .Release.Name }}-cb-backup"
            envFrom:
            - secretRef:
                name: cb-auth
            env:
              - name: APP_COUCHBASE_HOSTNAME
                value: "{{ .Release.Name }}-couchbase"
            image: {{ .Values.cluster.image }}:{{ .Values.cluster.tag }}
            command: ["bash", "/config/cb_backup.sh"]
            volumeMounts:
              - mountPath: "/backups"
                name: {{ .Release.Name }}-cb-backup-data
              - name: config-data
                mountPath: /config
          restartPolicy: Never
          volumes:
            - name: {{ .Release.Name }}-cb-backup-data
              persistentVolumeClaim:
                claimName: {{ .Release.Name }}-cb-backup-data-claim
            - name: config-data
              configMap:
                name: {{ .Release.Name }}-configure-scripts
---
# Used to inspect backup volume
kind: Job
apiVersion: batch/v1
metadata:
  name: {{ .Release.Name }}-cb-backup-inspect
spec:
  template:
    metadata:
      labels:
        type: cb-backup
    spec:
      {{- if .Values.global.nodePool }}
      nodeSelector:
        cloud.google.com/gke-nodepool: {{ .Values.nodeSpecs.nodeType }}-{{ .Release.Namespace }}-pool
      {{- end }}
      {{- if .Values.global.taint }}
      tolerations:
        - key: {{ .Release.Namespace }}
          operator: Exists
          effect: NoSchedule
      {{- end }}
      affinity:
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: type
                operator: In
                values:
                - cb-backup
            topologyKey: "kubernetes.io/hostname"
      containers:
        - name: {{ .Release.Name }}-cb-backup-inspect
          envFrom:
          - secretRef:
              name: cb-auth
          env:
            - name: APP_COUCHBASE_HOSTNAME
              value: "{{ .Release.Name }}-couchbase"
          image: {{ .Values.cluster.image }}
          version: {{ .Values.cluster.tag }}
          command: ["tail", "-f", "/dev/null"]
          volumeMounts:
            - name: {{ .Release.Name }}-cb-backup-data
              mountPath: "/backups"
      restartPolicy: Never
      volumes:
        - name: {{ .Release.Name }}-cb-backup-data
          persistentVolumeClaim:
            claimName: {{ .Release.Name }}-cb-backup-data-claim
#---
## Delete lock
#kind: Job
#apiVersion: batch/v1
#metadata:
#  name: {{ .Release.Name }}-cb-backup-delete-lock
#spec:
#  template:
#    metadata:
#      labels:
#        type: cb-backup
#    spec:
#      {{- if .Values.global.nodePool }}
#      nodeSelector:
#        cloud.google.com/gke-nodepool: {{ .Values.nodeSpecs.nodeType }}-{{ .Release.Namespace }}-pool
#      {{- end }}
#      {{- if .Values.global.taint }}
#      tolerations:
#        - key: {{ .Release.Namespace }}
#          operator: Exists
#          effect: NoSchedule
#      {{- end }}
#      affinity:
#        podAffinity:
#          requiredDuringSchedulingIgnoredDuringExecution:
#          - labelSelector:
#              matchExpressions:
#              - key: type
#                operator: In
#                values:
#                - cb-backup
#            topologyKey: "kubernetes.io/hostname"
#      containers:
#        - name: {{ .Release.Name }}-cb-backup-delete-lock
#          image: {{ .Values.cluster.image }}
#          version: {{ .Values.cluster.tag }}
#          command: ["rm", "-f", "/backups/cb/lock.lk"]
#          volumeMounts:
#            - name: {{ .Release.Name }}-cb-backup-data
#              mountPath: "/backups"
#      restartPolicy: Never
#      volumes:
#        - name: {{ .Release.Name }}-cb-backup-data
#          persistentVolumeClaim:
#            claimName: {{ .Release.Name }}-cb-backup-data-claim
{{- end }}
